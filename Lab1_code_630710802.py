import sklearn
from sklearn.decomposition import PCA
import numpy as np
from sklearn import datasets
import matplotlib.pyplot as plt

iris = datasets.load_iris()
X = iris.data
y = iris.target

print(X.shape)

# 2b
fig = plt.figure(1, figsize=(8, 6))
plt.clf()
plt.cla()
plt.scatter(X[:, 0], X[:, 1])
plt.show()

# 2c
pca = PCA(n_components=2)

# 2d
pca.fit(X)
X1 = pca.transform(X)
print(X1.shape)
fig = plt.figure(1, figsize=(8, 6))
plt.clf()
plt.cla()
plt.scatter(X1[:, 0], X1[:, 1])
plt.show()

#2e
pca = None  # cancle pca แรก
pca = PCA(n_components=4)
pca.fit(X)
X1 = pca.transform(X)
var_rat = pca.explained_variance_ratio_
print(var_rat)
#num_com = pca.n_components_
#print(pca.n_components_)
#PC_value = np.arange(num_com)+1
#print(PC_value)
PC_values = np.arange(pca.n_components_) + 1
print(pca.n_components_)
print(PC_values)
plt.plot(PC_values, pca.explained_variance_ratio_, 'o-', linewidth=2, color='blue')
plt.title('Scree Plot')
plt.xlabel('Principal Component')
plt.ylabel('Variance Explained')
plt.show()


################  breast cancer wisconsin  ####################
breast = datasets.load_breast_cancer()
x = breast.data
print(x.shape)

#  scatter plot ก่อน pca
fig = plt.figure(1, figsize=(8, 6))
plt.clf()
plt.cla()
plt.scatter(x[:, 0], x[:, 1])
plt.show()

#  เลือกจำนวน compponents
pca = None
pca = PCA(n_components=4)
pca.fit(x)
x1 = pca.transform(x)
print(x1.shape)

#  scatter plot หลังจากทำ pca
fig = plt.figure(1, figsize=(8, 6))
plt.clf()
plt.cla()
plt.scatter(x1[:, 1], x1[:, 3])
plt.show()

#  Scree plot
pca.fit(x)
x1 = pca.transform(x)
PCA_values = np.arange(pca.n_components_) + 1
print(pca.n_components_)
print(PCA_values)
var = pca.explained_variance_ratio_
plt.plot(PCA_values, var, 'o-', linewidth=2, color='red')
plt.title('Scree Plot')
plt.xlabel('Principal Component')
plt.ylabel('Variance Explained')
plt.show()
